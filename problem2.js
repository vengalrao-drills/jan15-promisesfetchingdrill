// 2. Fetch all the todos
const getTodos = (callback) => {
    fetch("https://jsonplaceholder.typicode.com/todos") // fetching the data
        .then((response) => {
            if (!response.ok) {
                throw new Error("data not fetched");  
            }
            return response.json();
        })
        .then((data) => {
            callback('', data);
        })
        .catch((err) => {
            console.log(err);
        })
}

getTodos((err, data) => {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
})