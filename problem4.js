// 4. Use the promise chain and fetch the users first and then all the details for each user.

const getUser = () => {
  return new Promise((resolve, reject) => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Users data not fetched");
        } else {
          return response.json();
        }
      })
      .then((data) => {
        resolve(data);
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

const getAllDetails = (data) => {
  return new Promise((resolve, reject) => {
    let promises = data.map((current) => {
      // return fetch(`https://jsonplaceholder.typicode.com/users/${current.id}`)
      return fetch(
        `https://jsonplaceholder.typicode.com/users?id=${current.id}` // fetched the details of the each user
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error("");
          } else {
            return response.json(); // sending the data -> callback
          }
        })
        .catch((err) => {
          return err;
        });
    });
    // console.log(promises)
    Promise.all(promises) // all the fetched data are in promises. niw resolving the promises.
      .then((details) => {
        // console.log(data)
        try {
          let answer = data.reduce((accumulator, current, index) => {
            accumulator[current.id] = details[index];
            // console.log(current.id)
            return accumulator;
          }, {});
          resolve(answer);
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

getUser()
  .then((data) => {
    // fetchted all the users
    // console.log(data);
    return getAllDetails(data);
  })
  .catch((err) => {
    // console.log(err);
    return getAllDetails(err);
  })
  .then((data) => {
    console.log("data recievd ", data);
  })
  .catch((err) => {
    console.log(err);
  });
