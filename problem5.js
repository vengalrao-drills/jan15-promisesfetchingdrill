// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

const firstTodo = () => {
  return new Promise((resolve, reject) => {
    fetch(`https://jsonplaceholder.typicode.com/todos/1`) // fetching the data
      .then((response) => {
        if (!response.ok) {
          throw new Error("Data not fetcted");
        } else {
          return response.json(); // changing json format
        }
      })
      .then((data) => {
        // console.log(data)
        resolve(data);
      })
      .catch((err) => {
        console.log(err);
      });
  });
};

const getDetails = (id) => {
  return new Promise((resolve, reject) => {
    // fetch(`https://jsonplaceholder.typicode.com/users/${id}`) // checking with id
    fetch(`https://jsonplaceholder.typicode.com/users?id=${id}`) // checking with id
      .then((response) => {
        if (!response.ok) {
          throw new Error();
        } else {
          return response.json();
        }
      })
      .then((data) => {
        // console.log(data)
        resolve(data);
      })
      .catch((err) => {
        reject(data);
      });
  });
};

firstTodo()
  .then((data) => { // got the users data
    // console.log(data)
    return getDetails(data.userId); // now particularly  fetching the details associated with
  })
  .then((data) => {
    console.log(data);
  })
  .catch((err) => {
    console.log(err);
  });
