// 1. Fetch all the users

const getUser = (callback) => {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => {
            // console.log(response.status, response.ok)

            // RESPONSE -> this has all information about the response. This is a HTTP request making
            // whether the data is fetched or rejected.
            // if data is fetched ->>   response.status --- ranges in 200-299
            // if the data is fetched ->> response.ok --- True
            // if not fetched , It will be false
            if (!response.ok) {
                throw new Error("data not fetched");
            }
            return response.json(); // converting to json
        })
        .then((users) => callback(null, users)) // sending the data -> callback
        .catch((error) => {
            callback(error);
        });
};

getUser((error, data) => {
    if (error) {
        console.log(error); 
    } else {
        console.log(data);
        console.log("\nFetched all the Users");
    }
});
