// 3. Use the promise chain and fetch the users first and then the todos.

const getUsers = (cb) => {
  return new Promise((resolve, reject) => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Users Data not fetched");
        }
        return response.json();
      })
      .then((data) => {
        resolve(data); //resolved the data
      })
      .catch((err) => {
        reject(err); // rejected the data
      });
  });
};

const getTodos = (users) => {
  return new Promise((resolve, reject) => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Todos Data not fetched");
        }
        return response.json();
      })
      .then((data) => {
        try {
          //   resolve(data);
          // console.log(users)
          let answer = users.reduce((accumulator, current) => {
            // console.log(current.id)
            data.forEach((todo) => {
              if (current.id == todo.userId) {
                //checking induvidual user id.
                if (accumulator.hasOwnProperty(current.id) == false) {
                  accumulator[current.id] = [];
                } else {
                  accumulator[current.id].push(todo); // pushing the todos to that
                }
              }
            });
            return accumulator;
          }, {});
          resolve(answer);
        } catch (err) {
          reject(err);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};

getUsers()
  .then((data) => {
    //   console.log("Users Data ",data, "\n");
    console.log(data); // fetched the users.
    return getTodos(data); /// calling the todos
  })
  .catch((err) => {
    console.log("Users - ", err, "\n");
    return getTodos();
  })
  .then((data) => {
    console.log("Todos Data ", data, "\n");
  })
  .catch((err) => {
    console.log("Todos - ", err, "\n");
  });
